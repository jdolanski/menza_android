package tvz.j2.menza_android;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class PregledActivity extends AppCompatActivity {

    String urlPopisRestorana = "http://www.sczg.unizg.hr/prehrana/";
    String urlPrefiksLinkaRestorana = "http://www.sczg.unizg.hr/prehrana/restorani/";
    String restorani, linkNaRestoran = "", imeRestorana = "";
    Elements restoraniElements;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pregled);

        new GetPopisRestorana().execute(urlPopisRestorana);

        ListView listViewPopisRestorana = (ListView) findViewById(R.id.listViewRestorani);
        if (listViewPopisRestorana != null) {
            listViewPopisRestorana.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    // micanje palatala, točaka i razmaka za dobivanje linka
                    linkNaRestoran = urlPrefiksLinkaRestorana + ((TextView) view).getText().toString().toLowerCase()
                            .replace(".", "")
                            .replace(" ", "-")
                            .replace("š", "s")
                            .replace("ć", "c");
                    imeRestorana = ((TextView) view).getText().toString();

                    // pokretanje pregleda pojedinog restorana
                    Intent intent = new Intent(getApplicationContext(), RestoranPregledActivity.class);
                    intent.putExtra("linkRestorana", linkNaRestoran);
                    intent.putExtra("imeRestorana", imeRestorana);
                    startActivity(intent);
                }
            });
        }
    }

    class GetPopisRestorana extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(PregledActivity.this, "Dohvat popisa restorana", "Dohvaćam podatke");
        }

        @Override
        protected String doInBackground(String... params) {
            Document dokumentPopisaRestorana;

            try {
                dokumentPopisaRestorana = Jsoup.connect("http://www.sczg.unizg.hr/prehrana/").get();
                restoraniElements = dokumentPopisaRestorana.getElementsByClass("title");
            } catch (IOException e) {
                e.printStackTrace();
            }

            return restorani;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();

            // traženje imena restorana
            ArrayList<String> imenaRestorana = new ArrayList<>();

            for (Element e : restoraniElements) {
                imenaRestorana.add(e.text());
            }

            // postavljanje imena u listu
            ListView listView = (ListView) findViewById(R.id.listViewRestorani);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(PregledActivity.this, android.R.layout.simple_list_item_1, imenaRestorana);
            if (listView != null) {
                listView.setAdapter(adapter);
            }
        }
    }
}

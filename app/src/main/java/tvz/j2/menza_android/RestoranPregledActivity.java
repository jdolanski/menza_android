package tvz.j2.menza_android;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tvz.j2.menza_android.tvz.j2.menza_android.db.AsyncResponse;
import tvz.j2.menza_android.tvz.j2.menza_android.db.Menza;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaUtility;

public class RestoranPregledActivity extends AppCompatActivity {

    String linkNaRestoran, radnoVrijemeHtml, jelovnikHtml, linkNaSliku, imeRestorana, brojLjudi, vrijemeAzuriranjaUnix;
    TextView textViewBrojLjudi, textViewVrijemeAzuriranja;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restoran_pregled);

        Bundle bundle = getIntent().getExtras();
        linkNaRestoran = bundle.getString("linkRestorana");
        imeRestorana = bundle.getString("imeRestorana");

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(imeRestorana);
        }

        textViewBrojLjudi = (TextView) findViewById(R.id.textViewBrojLjudi);
        textViewVrijemeAzuriranja = (TextView) findViewById(R.id.textViewVrijemeAzuriranja);
        if (textViewBrojLjudi != null) {

            MenzaUtility.getMenzaByNaziv(imeRestorana.replace(" ", "%20"), new AsyncResponse() {
                @Override
                public void processFinish(Menza menza) {
                    brojLjudi = menza.getBr_ljudi();
                    linkNaSliku = menza.getSlika_ref();
                    vrijemeAzuriranjaUnix = menza.getTimestamp();

                    textViewBrojLjudi.setText(getString(R.string.broj_ljudi_set, brojLjudi));

                    if(vrijemeAzuriranjaUnix != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss", Locale.US);
                        Date vrijemeTest = new Date(Long.parseLong(vrijemeAzuriranjaUnix) * 1000);
                        textViewVrijemeAzuriranja.setText(getString(R.string.vrijemeAzuriranjaSet, sdf.format(vrijemeTest)));
                    } else {
                        textViewVrijemeAzuriranja.setText(R.string.menzaNijeAzurirana);
                    }


                    Uri uri = Uri.parse(linkNaSliku);
                    SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.imageView);
                    if (simpleDraweeView != null) {
                        simpleDraweeView.setImageURI(uri);
                    }
                }
            });
        }

        RadioButton radioButtonRadno = (RadioButton) findViewById(R.id.radnoVrijemeToggle);
        RadioButton radioButtonJelovnik = (RadioButton) findViewById(R.id.jelovnikToggle);

        if (radioButtonJelovnik != null) {
            if(radioButtonJelovnik.isChecked()) {
                new GetJelovnik().execute(linkNaRestoran);
            } else {
                new GetRadnoVrijeme().execute(linkNaRestoran);
            }
        }

        if (radioButtonRadno != null) {
            radioButtonRadno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetRadnoVrijeme().execute(linkNaRestoran);
                }
            });
        }

        if (radioButtonJelovnik != null) {
            radioButtonJelovnik.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetJelovnik().execute(linkNaRestoran);
                }
            });
        }
    }

    class GetRadnoVrijeme extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(RestoranPregledActivity.this);
            progressDialog.setMessage("Dohvat podataka o menzi");
        }

        @Override
        protected String doInBackground(String... params) {
            Document dokumentZaOpis;

            if(radnoVrijemeHtml == null || radnoVrijemeHtml.isEmpty()) {
                try {
                    dokumentZaOpis = Jsoup.connect(linkNaRestoran).get();
                    radnoVrijemeHtml = dokumentZaOpis.select(".content").get(0).html();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return linkNaRestoran;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();

            webView = (WebView) findViewById(R.id.webView);
            if (webView != null) {
                webView.loadDataWithBaseURL(null, radnoVrijemeHtml, "text/html", "uft-8", null);
            }
        }
    }

    class GetJelovnik extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(RestoranPregledActivity.this);
            progressDialog.setMessage("Dohvat jelovnika");
        }

        @Override
        protected String doInBackground(String... params) {
            Document dokumentZaOpis;

            if(jelovnikHtml == null || jelovnikHtml.isEmpty()) {
                try {
                    dokumentZaOpis = Jsoup.connect(linkNaRestoran).get();
                    jelovnikHtml = dokumentZaOpis.select(".content").get(1).html();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return linkNaRestoran;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            progressDialog.dismiss();

            webView = (WebView) findViewById(R.id.webView);
            if (webView != null) {
                webView.loadDataWithBaseURL(null, jelovnikHtml, "text/html", "uft-8", null);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // briše cache iz fresca da prikaže nove slike iz baze
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearCaches();
    }
}

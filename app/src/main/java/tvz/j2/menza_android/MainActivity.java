package tvz.j2.menza_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.drawee.backends.pipeline.Fresco;

public class MainActivity extends AppCompatActivity {

    Button dojavaBttn;
    Button pregledBttn;
    Button provjeraStanjaBttn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fresco.initialize(this);

        dojavaBttn = (Button) findViewById(R.id.bttn_activity_main_dojava);
        pregledBttn = (Button) findViewById(R.id.bttn_activity_main_pregled);
        provjeraStanjaBttn = (Button) findViewById(R.id.bttn_activity_main_provjera_stanja);

        // Create onClick listeners
        View.OnClickListener dojavaBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DojavaActivity.class);
                startActivity(intent);
            }
        };

        View.OnClickListener pregledBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PregledActivity.class);
                startActivity(intent);
            }
        };

        View.OnClickListener provjeraStanjaBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProvjeraStanjaActivity.class);
                startActivity(intent);
            }
        };

        // test commita

        // Bind onClick listeners
        dojavaBttn.setOnClickListener(dojavaBttnListener);
        pregledBttn.setOnClickListener(pregledBttnListener);
        provjeraStanjaBttn.setOnClickListener(provjeraStanjaBttnListener);
    }
}

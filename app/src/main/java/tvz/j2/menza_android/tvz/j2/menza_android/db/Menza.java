package tvz.j2.menza_android.tvz.j2.menza_android.db;

public class Menza {
    private String id;
    private String naziv;
    private String br_ljudi;
    private String slika_ref;
    private String timestamp;

    public Menza() {

    }

    public Menza(String id, String naziv, String br_ljudi, String slika_ref, String timestamp) {
        this.id = id;
        this.naziv = naziv;
        this.br_ljudi = br_ljudi;
        this.slika_ref = slika_ref;
        this.timestamp = timestamp;
    }

    public Menza(Menza men) {
        this.id = men.getId();
        this.naziv = men.getNaziv();
        this.br_ljudi = men.getBr_ljudi();
        this.slika_ref = men.getSlika_ref();
        this.timestamp = men.getTimestamp();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getBr_ljudi() {
        return br_ljudi;
    }

    public void setBr_ljudi(String br_ljudi) {
        this.br_ljudi = br_ljudi;
    }

    public String getSlika_ref() {
        return slika_ref;
    }

    public void setSlika_ref(String slika_ref) {
        this.slika_ref = slika_ref;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

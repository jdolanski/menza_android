package tvz.j2.menza_android.tvz.j2.menza_android.db;

public final class MenzaMySQLConfig {
    MenzaMySQLConfig() {}

    // Address of our scripts of the CRUD
    public static final String URL_GET_ALL_MENZA            = "http://menza.net16.net/index.php";
    public static final String URL_GET_MENZA_BY_ID          = "http://menza.net16.net/get_menza_by_id.php?id=";
    public static final String URL_GET_MENZA_BY_NAZIV       = "http://menza.net16.net/get_menza_by_naziv.php?naziv=";
    public static final String URL_GET_ALL_MENZA_ID_NAZIV   = "http://menza.net16.net/get_all_menza_id_naziv.php";

    public static final String URL_UPLOAD_IMAGE             = "http://menza.net16.net/upload_image.php";

    // Updates by id
    public static final String URL_UPDATE_MENZA             = "http://menza.net16.net/update_menza.php";

    // Keys that will be used to send the request to php scripts
    public static final String KEY_MENZA_ID                 = "id";
    public static final String KEY_MENZA_NAZIV              = "naziv";
    public static final String KEY_MENZA_BR_LJUDI           = "br_ljudi";
    public static final String KEY_MENZA_SLIKA_REF          = "slika_ref";
    public static final String KEY_MENZA_TIMESTAMP          = "timestamp";

    public static final String KEY_IMAGE                    = "image";

    // JSON Tags
    public static final String TAG_JSON_ARRAY               = "result";
    public static final String TAG_ID                       = "id";
    public static final String TAG_NAZIV                    = "naziv";
    public static final String TAG_BR_LJUDI                 = "br_ljudi";
    public static final String TAG_SLIKA_REF                = "slika_ref";
    public static final String TAG_MENZA_TIMESTAMP          = "timestamp";
}

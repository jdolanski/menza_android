package tvz.j2.menza_android.tvz.j2.menza_android.db;

/**
 * Interface kojem je parametar output od onPostExecute-a
 * Primjer koristenja:
 *  MenzaUtility.getMenzaByNaziv("FSB", new AsyncResponse() {
 *      @Override
 *      public void processFinish(Menza output) {
 *          testmenzaNaz = output;
 *          odabirMenzeBttn.setEnabled(true);
 *      }
 *  });
 */
public interface AsyncResponse {
    void processFinish(Menza output);
}

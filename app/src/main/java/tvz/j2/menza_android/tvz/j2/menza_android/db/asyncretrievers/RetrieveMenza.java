package tvz.j2.menza_android.tvz.j2.menza_android.db.asyncretrievers;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tvz.j2.menza_android.tvz.j2.menza_android.db.AsyncResponse;
import tvz.j2.menza_android.tvz.j2.menza_android.db.Menza;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaMySQLConfig;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaRequestHandler;

public class RetrieveMenza extends AsyncTask<String, Void, String> {
    private AsyncResponse listener;
    private String retrieveByWhat;

    public RetrieveMenza(AsyncResponse listener, String byWhat) {
        this.listener = listener;
        this.retrieveByWhat = byWhat;
    }

    @Override
    protected String doInBackground(String... params) {
        return MenzaRequestHandler.sendGetRequestParam(retrieveByWhat, params[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        // Uncomment to debug
        System.out.println(s);

        JSONObject jsonObject;
        Menza menza = null;
        try {
            jsonObject = new JSONObject(s);
            JSONArray result = jsonObject.getJSONArray(MenzaMySQLConfig.TAG_JSON_ARRAY);

            for (int i = 0; i < result.length(); ++i) {
                JSONObject tmpJo = result.getJSONObject(i);
                String id = tmpJo.getString(MenzaMySQLConfig.TAG_ID);
                String naziv = tmpJo.getString(MenzaMySQLConfig.TAG_NAZIV);
                String br_ljudi = tmpJo.getString(MenzaMySQLConfig.TAG_BR_LJUDI);
                String slika_ref = tmpJo.getString(MenzaMySQLConfig.TAG_SLIKA_REF);
                String timestamp = tmpJo.getString(MenzaMySQLConfig.TAG_MENZA_TIMESTAMP);
                menza = new Menza(id, naziv, br_ljudi, slika_ref, timestamp);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            listener.processFinish(menza);
        }

    }
}
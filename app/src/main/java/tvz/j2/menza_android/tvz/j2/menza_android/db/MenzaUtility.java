package tvz.j2.menza_android.tvz.j2.menza_android.db;


import android.os.AsyncTask;

import java.util.HashMap;

import tvz.j2.menza_android.tvz.j2.menza_android.db.asyncretrievers.RetrieveAllMenza;
import tvz.j2.menza_android.tvz.j2.menza_android.db.asyncretrievers.RetrieveMenza;

public class MenzaUtility {
    public MenzaUtility() {
    }

    /**
     * Verzija koja javlja kada je zavrseno
     *
     * @param asyncResponseList implementirani interface koji se aktivira pri zavrsetku onPostExecute-a i vraca njegov rezultat(Listu menza)
     */
    public static void getAllMenza(final AsyncResponseList asyncResponseList) {
        RetrieveAllMenza retrieveAllMenza = new RetrieveAllMenza(asyncResponseList);
        retrieveAllMenza.execute();
    }


    /**
     * Verzija koja javlja kada je zavrseno
     *
     * @param id            ID menze
     * @param asyncResponse implementirani interface koji se aktivira pri zavrsetku onPostExecute-a i vraca njegov rezultat (menzu)
     */
    public static void getMenzaById(final String id, final AsyncResponse asyncResponse) {
        RetrieveMenza retrieveMenza = new RetrieveMenza(asyncResponse, MenzaMySQLConfig.URL_GET_MENZA_BY_ID);
        retrieveMenza.execute(id);
    }


    /**
     * Verzija koja javlja kada je zavrseno
     *
     * @param naziv         Naziv menze
     * @param asyncResponse implementirani interface koji se aktivira pri zavrsetku onPostExecute-a i vraca njegov rezultat (menzu)
     */
    public static void getMenzaByNaziv(final String naziv, final AsyncResponse asyncResponse) {
        RetrieveMenza retrieveMenza = new RetrieveMenza(asyncResponse, MenzaMySQLConfig.URL_GET_MENZA_BY_NAZIV);
        retrieveMenza.execute(naziv);
    }


    /**
     * @param id      ID menze
     * @param brLjudi Br ljudi
     */
    public static void updateMenzaById(final String id, final String brLjudi) {

        class UpdateMenza extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> data = new HashMap<>();
                data.put(MenzaMySQLConfig.KEY_MENZA_ID, id);
                data.put(MenzaMySQLConfig.KEY_MENZA_BR_LJUDI, brLjudi);

                return MenzaRequestHandler.sendPostRequest(MenzaMySQLConfig.URL_UPDATE_MENZA, data);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                System.out.println(s);
            }
        }
        UpdateMenza updateMenza = new UpdateMenza();
        updateMenza.execute();
    }

    /**
     * Verzija koja javlja kada je zavrseno
     *
     * @param id      ID menze
     * @param brLjudi Br ljudi
     * @param otc     implementirani interface koji se aktivira pri zavrsetku onPostExecute-a
     */
    public static void updateMenzaById(final String id, final String brLjudi, final OnTaskCompleted otc) {

        class UpdateMenza extends AsyncTask<Void, Void, String> {
            private OnTaskCompleted listener;

            public UpdateMenza(OnTaskCompleted listener) {
                this.listener = listener;
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String, String> data = new HashMap<>();
                data.put(MenzaMySQLConfig.KEY_MENZA_ID, id);
                data.put(MenzaMySQLConfig.KEY_MENZA_BR_LJUDI, brLjudi);

                return MenzaRequestHandler.sendPostRequest(MenzaMySQLConfig.URL_UPDATE_MENZA, data);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                System.out.println(s);

                listener.onTaskCompleted();
            }
        }
        UpdateMenza updateMenza = new UpdateMenza(otc);
        updateMenza.execute();
    }

}

package tvz.j2.menza_android.tvz.j2.menza_android.db;

import java.util.List;

/**
 * Isto kao AsyncResponse ali vraca listu
 */
public interface AsyncResponseList {
    void processFinishedFetchingList(List<Menza> output);
}

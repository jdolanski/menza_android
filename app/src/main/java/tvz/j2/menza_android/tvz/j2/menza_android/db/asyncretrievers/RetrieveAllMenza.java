package tvz.j2.menza_android.tvz.j2.menza_android.db.asyncretrievers;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tvz.j2.menza_android.tvz.j2.menza_android.db.AsyncResponseList;
import tvz.j2.menza_android.tvz.j2.menza_android.db.Menza;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaMySQLConfig;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaRequestHandler;

public class RetrieveAllMenza extends AsyncTask<Void, Void, String> {
    private AsyncResponseList listener;

    public RetrieveAllMenza(AsyncResponseList listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        return MenzaRequestHandler.sendGetRequest(MenzaMySQLConfig.URL_GET_ALL_MENZA);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        // Uncomment to debug
//        System.out.println(s);

        JSONObject jsonObject;
        List<Menza> menze = new ArrayList<>();
        try {
            jsonObject = new JSONObject(s);
            JSONArray result = jsonObject.getJSONArray(MenzaMySQLConfig.TAG_JSON_ARRAY);

            for (int i = 0; i < result.length(); ++i) {
                JSONObject tmpJo = result.getJSONObject(i);
                String id = tmpJo.getString(MenzaMySQLConfig.TAG_ID);
                String naziv = tmpJo.getString(MenzaMySQLConfig.TAG_NAZIV);
                String br_ljudi = tmpJo.getString(MenzaMySQLConfig.TAG_BR_LJUDI);
                String slika_ref = tmpJo.getString(MenzaMySQLConfig.TAG_SLIKA_REF);
                String timestamp = tmpJo.getString(MenzaMySQLConfig.TAG_MENZA_TIMESTAMP);
                Menza tmpMenza = new Menza(id, naziv, br_ljudi, slika_ref, timestamp);
                menze.add(tmpMenza);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            listener.processFinishedFetchingList(menze);
        }
    }
}
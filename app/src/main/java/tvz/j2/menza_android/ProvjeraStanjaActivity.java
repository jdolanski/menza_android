package tvz.j2.menza_android;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;


public class ProvjeraStanjaActivity extends AppCompatActivity {

    EditText brojIksiceField;
    Button provjeriBttn;
    TextView stanjeTv;
    String brojIksice, stanje;
    String[] komadi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provjera_stanja);

        brojIksiceField = (EditText) findViewById(R.id.editTextBrojIksice);
        provjeriBttn = (Button) findViewById(R.id.bttn_activity_provjera_stanja_provjeri);
        brojIksiceField.setText(R.string.pocetniBrojIksice); // dio koji je isti na svima
        stanjeTv = (TextView) findViewById(R.id.textViewStanje);
        // prikaz tipkovnice kod otvaranja
        brojIksiceField.setSelection(brojIksiceField.getText().length());
        brojIksiceField.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        View.OnClickListener provjeriBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brojIksice = brojIksiceField.getEditableText().toString();

                sendPostRequest(brojIksice);
            }
        };

        provjeriBttn.setOnClickListener(provjeriBttnListener);
    }

    public void sendPostRequest(final String brojIksice) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            ProgressDialog progress;

            protected void onPreExecute() {
                progress = new ProgressDialog(ProvjeraStanjaActivity.this);
                progress.setMessage("Provjera stanja iksice");
                progress.show();
            }

            @Override
            protected String doInBackground(String... params) {

                Document document;

                try {/*
                        Početni link: http://wap.cap.srce.hr
                        Generirani prvi dio: http:/wap.cap.srce.hr/(S(yfmdmy551sroisfkxvulkg45))/Wapx.aspx
                        Generirani drugi dio: http://wap.cap.srce.hr/(S(jj2hivqluhyka2fyb2yuvi55))/Wapx.aspx?__ufps=701754
                        Argumenti: __EVENTTARGET="", __EVENTARGUMENT="",brkartice=BROJ_IKSICE,Command1=Dohvati
                    */

                    // spajanje za dohvat prvog generiranog dijela linka
                    Connection.Response response = Jsoup.connect("http://wap.cap.srce.hr")
                            .followRedirects(true)
                            .method(Connection.Method.POST)
                            .execute();

                    String prviDioLinka = response.url().toString();

                    // dohvat drugog generiranog dijela linka
                    document = Jsoup.connect(prviDioLinka)
                            .get();

                    String prviAtribut = document.getElementsByAttribute("action").attr("action");
                    prviAtribut = prviAtribut.substring(9); // micanje "Wapx.aspx?" dijela

                    String konacniLink = prviDioLinka + prviAtribut + "&__EVENTTARGET=&__EVENTARGUMENT=&brkartice=" + brojIksice + "&Command1=Dohvati";
                    document = Jsoup.connect(konacniLink)
                            .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
                            .get();

                    stanje = document.text();

                    // razdvajanje po recima
                    komadi = stanje.split("\\.\\s+");

                    for (String s : komadi) {
                        stanje += s + "\n";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progress.dismiss();

                stanjeTv.setText(stanje);
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(brojIksice);
    }
}

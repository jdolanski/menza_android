package tvz.j2.menza_android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import tvz.j2.menza_android.tvz.j2.menza_android.db.AsyncResponseList;
import tvz.j2.menza_android.tvz.j2.menza_android.db.Menza;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaMySQLConfig;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaRequestHandler;
import tvz.j2.menza_android.tvz.j2.menza_android.db.MenzaUtility;
import tvz.j2.menza_android.tvz.j2.menza_android.db.OnTaskCompleted;

public class DojavaActivity extends AppCompatActivity {
    boolean IMAGE_OK = false;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int PICK_IMAGE_REQUEST = 1;
    final CharSequence[] dialogOptions = new CharSequence[]{"Upload postojeće slike", "Pokreni kameru"};
    Bitmap bitmap;

    Button odabirMenzeBttn;
    Menza ODABRANA_MENZA;

    EditText brojLjudiEt;
    Button odabirSlikeBttn;
    ImageView imagePreview;

    Button potvrdiBttn;

    List<Menza> menzas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dojava);

        odabirMenzeBttn = (Button) findViewById(R.id.bttn_activity_dojava_odabir_menze);
        brojLjudiEt = (EditText) findViewById(R.id.et_activity_dojava_br_ljudi);
        odabirSlikeBttn = (Button) findViewById(R.id.bttn_activity_dojava_odabir_slike);
        potvrdiBttn = (Button) findViewById(R.id.bttn_activity_dojava_potvrdi);
        imagePreview = (ImageView) findViewById(R.id.iv_activity_dojava_preview_image);

        odabirMenzeBttn.setBackgroundColor(Color.DKGRAY);
        potvrdiBttn.setBackgroundColor(Color.DKGRAY);

        // Ako je skidanje zavrsilo enablaj button
        MenzaUtility.getAllMenza(new AsyncResponseList() {
            @Override
            public void processFinishedFetchingList(List<Menza> output) {
                menzas = output;

                Collections.sort(menzas, new Comparator<Menza>() {
                    @Override
                    public int compare(Menza lhs, Menza rhs) {
                        return Integer.valueOf(lhs.getId()).compareTo(Integer.valueOf(rhs.getId()));
                    }
                });
                odabirMenzeBttn.setEnabled(true);
                odabirMenzeBttn.setBackgroundColor(Color.rgb(63, 81, 181));

            }
        });

        // Create onClick listeners
        final View.OnClickListener odabirMenzeBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence[] nazivi = new CharSequence[menzas.size()];
                for (int i = 0; i < menzas.size(); ++i) {
                    nazivi[i] = menzas.get(i).getNaziv();
                }
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(v.getContext());
                alertBuilder.setItems(nazivi, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ODABRANA_MENZA = menzas.get(which);
                        odabirMenzeBttn.setText(ODABRANA_MENZA.getNaziv());
                        isOkToUnlockUpdate();
                    }
                });
                alertBuilder.show();
            }
        };

        final View.OnClickListener odabirSlikeBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Odabir slike za upload na bazu. Pop-up menu pita zelimo li upload postojecu slku ili uslikati
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Odaberite");
                builder.setItems(dialogOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            // Upload postojece
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                        } else if (which == 1) {
                            // Pokretanje kamere
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (intent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                            }
                        }
                    }
                });
                builder.show();
            }
        };

        final View.OnClickListener potvrdiBttnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    System.out.println(ODABRANA_MENZA.getId());

                    MenzaUtility.updateMenzaById(ODABRANA_MENZA.getId(), brojLjudiEt.getText().toString(), new OnTaskCompleted() {
                        @Override
                        public void onTaskCompleted() {
                            Toast.makeText(DojavaActivity.this, "Dojavljeno", Toast.LENGTH_SHORT).show();
                            // povratak na početni ekran
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                        }
                    });
                    // Upload slike ako je odabrana
                    if (IMAGE_OK) uploadImage();
                }
        };

        // Bind onClick listeners
        odabirMenzeBttn.setOnClickListener(odabirMenzeBttnListener);
        odabirSlikeBttn.setOnClickListener(odabirSlikeBttnListener);

        // EditText change listener
        brojLjudiEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                isOkToUnlockUpdate();
            }
        });

        potvrdiBttn.setOnClickListener(potvrdiBttnListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                // Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // Setting the Bitmap to ImageView
                imagePreview.setImageBitmap(bitmap);
                float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();
                int width = 480;
                int height = Math.round(width / aspectRatio);
                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

                IMAGE_OK = true;
            } catch (IOException e) {
                e.printStackTrace();
                IMAGE_OK = false;
            }
        }
    }

    private void uploadImage() {
        // Showing the progress dialog
        final ProgressDialog progress = new ProgressDialog(DojavaActivity.this);
        progress.setMessage("Upload slike...");
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MenzaMySQLConfig.URL_UPLOAD_IMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                progress.dismiss();
                //Toast.makeText(DojavaActivity.this, "Dojavljeno", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progress.dismiss();
                //Toast.makeText(DojavaActivity.this, volleyError.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Converting Bitmap to String
                String image = MenzaRequestHandler.getStringImage(bitmap);

                // Creating parameters
                Map<String, String> params = new Hashtable<>();

                // Adding parameters
                params.put(MenzaMySQLConfig.KEY_IMAGE, image);
                params.put(MenzaMySQLConfig.KEY_MENZA_ID, ODABRANA_MENZA.getId());

                // Returning parameters
                return params;
            }
        };


        // sprječavanje timeouta
        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 3;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });

        // Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Adding request to the queue
        requestQueue.add(stringRequest);
    }


    public void isOkToUnlockUpdate() {
        // If it's ok to update DB: All parameters (id, br_ljudi or image must be selected)
        boolean ID_MENZA_OK;
        boolean BR_LJUDI_OK;
        boolean UPDATE_ISOK = false;
        if (ODABRANA_MENZA != null && !Objects.equals(brojLjudiEt.getText().toString(), "") && brojLjudiEt.getText() != null) {
            ID_MENZA_OK = ODABRANA_MENZA.getId() != null;
            int brLjudi = Integer.parseInt(brojLjudiEt.getText().toString());
            BR_LJUDI_OK = brLjudi > 0;

            UPDATE_ISOK = ID_MENZA_OK && BR_LJUDI_OK;
        }
        if (UPDATE_ISOK) {
            potvrdiBttn.setEnabled(true);
            potvrdiBttn.setBackgroundColor(Color.rgb(63, 81, 181));
        } else {
            potvrdiBttn.setEnabled(false);
            potvrdiBttn.setBackgroundColor(Color.DKGRAY);
        }
    }

}

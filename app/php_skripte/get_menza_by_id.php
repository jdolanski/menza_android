<?php
	require_once('db_connect.php');
	
	$id = $_GET['id'];
	
	$sql = "SELECT * FROM menza WHERE id=${id}";
	
	$r = mysqli_query($conn, $sql);
	
	$result = array();
	$row = mysqli_fetch_array($r);
	array_push($result, array(
			"id" => $row['id'],
			"naziv" => $row['naziv'],
			"br_ljudi" => $row['br_ljudi'],
			"slika_ref" => $row['slika_ref'],
			"timestamp" => $row['timestamp']
		)
	);
	
	// Displaying in json format
	echo json_encode(array('result' => $result));
	
	mysqli_close($conn);
?>
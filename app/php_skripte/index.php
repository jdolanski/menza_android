<?php
	require_once('db_connect.php');
	
	$sql = "SELECT * FROM menza";
	$r = mysqli_query($conn, $sql);
	$result = array();
	
	while ($row = mysqli_fetch_array($r)) {
		array_push($result, array("id" => $row['id'],
			"naziv" => $row['naziv'],
			"br_ljudi" => $row['br_ljudi'],
			"slika_ref" => $row['slika_ref'],
			"timestamp" => $row['timestamp']
		));
	}
	
	echo json_encode(array('result' => $result));
	
	mysqli_close($conn);
?>